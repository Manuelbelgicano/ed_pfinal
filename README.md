# Practica Final ED
Proyecto final de la asignatura Estructura de Computadores para la Universidad de Granada
***
El proyecto consiste en la creacion de un juego ejecutable desde la terminal basado en el popular Scrabble.

El objetivo era ser capaz de aplicar los conocimientos de la asignatura para crear primero el juego
de forma que pudiera jugarse desde la terminal y además añadir una IA contra la que jugar.

