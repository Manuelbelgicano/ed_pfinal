# Makefile de la práctica final de la asignatura de Estructura de Datos
# Autores: Manuel Gachs Ballegeer
#	   Gonzalo Moreno Soto
# Fecha: Diciembre 2018
# License: GPLv3

####################################################################
####		DECLARACIÓN DE VARIABLES Y DIRECTIVAS		####
####################################################################

# Compilador y flags
CXX := g++
CXXFLAGS := -g

# Variables de directorios
BIN := bin/
DATA := data/
DOC := doc/
INC := include/
OBJ = obj/
SRC := src/
TESTS := test/

# Variables de archivos
# Diccionario
DIC := diccionario
DIC_EXEC := $(BIN)$(DIC)_test
DIC_HDR := $(DIC).h
DIC_SRC := $(DIC).cpp
DIC_OBJ = $(DIC_SRC:.cpp=.o)
DIC_TEST := $(DIC)_test.cpp
DIC_TEST_OBJ := $(DIC_TEST:.cpp=.o)
# Letra
LETTER := letra
LETTER_EXEC := $(BIN)$(LETTER)_test
LETTER_HDR := $(LETTER).h
LETTER_SRC := $(LETTER).cpp
LETTER_OBJ = $(LETTER_SRC:.cpp=.o)
LETTER_TEST := $(LETTER)_test.cpp
LETTER_TEST_OBJ := $(LETTER_TEST:.cpp=.o)
# Conjunto de letras
L_LIST := lista_letras
L_LIST_EXEC := $(BIN)$(L_LIST)_test
L_LIST_HDR := $(L_LIST).h
L_LIST_SRC := $(L_LIST).cpp
L_LIST_OBJ = $(L_LIST_SRC:.cpp=.o)
L_LIST_TEST := $(L_LIST)_test.cpp
L_LIST_TEST_OBJ := $(L_LIST_TEST:.cpp=.o)
# Bolsa de letras
L_BAG := bolsa_letras
L_BAG_EXEC := $(BIN)$(L_BAG)_test
L_BAG_HDR := $(L_BAG).h
L_BAG_SRC := $(L_BAG).cpp
L_BAG_OBJ = $(L_BAG_SRC:.cpp=.o)
L_BAG_TEST := $(L_BAG)_test.cpp
L_BAG_TEST_OBJ := $(L_BAG_TEST:.cpp=.o)
# Inteligencia artificial
IA_PROJECT := ia
IA_EXEC := $(BIN)$(IA_PROJECT)_test
IA_HDR := $(IA_PROJECT).h
IA_SRC := $(IA_PROJECT).cpp
IA_OBJ := $(IA_SRC:.cpp=.o)
IA_TEST := $(IA_PROJECT)_test.cpp
IA_TEST_OBJ := $(IA_TEST:.cpp=.o)
# Cantidad de letras
L_FREC := cantidad_letras
L_FREC_EXEC := $(BIN)$(L_FREC)
L_FREC_SRC := $(L_FREC).cpp
L_FREC_OBJ := $(L_FREC_SRC:.cpp=.o)
# Juego
GAME := letras
GAME_EXEC := $(BIN)$(GAME)
GAME_HDR := auxiliares.h buscador_arbol.h
GAME_SRC := $(GAME_HDR:.h=.cpp)
GAME_OBJ := $(GAME_SRC:.cpp=.o)
MAIN_GAME_SRC := $(GAME).cpp
MAIN_GAME_OBJ := $(MAIN_GAME_SRC:.cpp=.o)

# Variables de reglas
define create_obj =
@echo "Compilando $@ ..."
@$(CXX) $(CXXFLAGS) -o $@ $<
@echo "${GREEN}Hecho${RESET}"
endef
define create_bin =
@echo "Creando el archivo ejecutable $@ ..."
@$(CXX) $(CXXFLAGS) -o $@ $^
@echo "${GREEN}Hecho${RESET}"
endef
define make_dir =
@echo "Creando directorio $@ ..."
@mkdir -p $@
@echo "${GREEN}Hecho${RESET}"
endef
define perform_test =
@echo "${BOLD}Realizando test para $<:${RESET}"
@./$< $(TEST)
endef

# Otras variables
TEST :=
DOCUMENTATION := doxygen
BROWSER := firefox
GREEN := `tput setaf 2`
BOLD := `tput bold`
RESET := `tput sgr0`

# Variables específicas a un patrón o regla
%.o: CXXFLAGS += -Wall -I./$(INC) -c
dic_test: TEST += $(wildcard $(DATA)$(DIC)*.*)
letter_list_test: TEST += $(wildcard $(DATA)$(L_LIST)*.*)
letter_bag_test: TEST += $(DATA)$(DIC)1.txt
ia_test: TEST += $(wildcard $(DATA)$(DIC)*.*)
l_frec_test: TEST+= $(DATA)$(DIC)1000.txt $(DATA)letras_test.txt $(TESTS)$(L_FREC)_test.txt

# Directivas
vpath %.h $(INC)
vpath %.cpp $(SRC)
vpath %.o $(OBJ)

####################################################################
####				REGLAS				####
####################################################################

# Reglas sin receta
all: l_frec game help author
dic: $(DIC_EXEC) 
letter: $(LETTER_EXEC)
letter_list: $(L_LIST_EXEC)
letter_bag: $(L_BAG_EXEC)
ia_project: $(IA_EXEC)
l_frec: $(L_FREC_EXEC)
game: $(GAME_EXEC)
.PHONY: clean $(DOCUMENTATION)
$(DIC_EXEC): | $(BIN) 
$(LETTER_EXEC): | $(BIN)
$(L_LIST_EXEC): | $(BIN)
$(L_BAG_EXEC): | $(BIN)
$(IA_EXEC): | $(BIN)
$(L_FREC_EXEC): | $(BIN)
$(GAME_EXEC): | $(BIN)
dic_test: | $(TESTS)
letter_list_test: | $(TESTS)
l_frec_test: | $(TESTS)

# Diccionario
$(DIC_EXEC): $(DIC_TEST_OBJ) $(DIC_OBJ)
	$(create_bin)

$(OBJ)$(DIC_OBJ): $(DIC_SRC) $(DIC_HDR) | $(OBJ)
	$(create_obj)

$(OBJ)$(DIC_TEST_OBJ): $(DIC_TEST) $(DIC_HDR) | $(OBJ)
	$(create_obj)

# Letra
$(LETTER_EXEC): $(LETTER_TEST_OBJ) $(LETTER_OBJ)
	$(create_bin)

$(OBJ)$(LETTER_OBJ): $(LETTER_SRC) $(LETTER_HDR) | $(OBJ)
	$(create_obj)

$(OBJ)$(LETTER_TEST_OBJ): $(LETTER_TEST) $(LETTER_HDR) | $(OBJ)
	$(create_obj)

# Conjunto de letras
$(L_LIST_EXEC): $(L_LIST_TEST_OBJ) $(L_LIST_OBJ) $(LETTER_OBJ) $(DIC_OBJ)
	$(create_bin)

$(OBJ)$(L_LIST_OBJ): $(L_LIST_SRC) $(L_LIST_HDR) $(LETTER_HDR) $(DIC_HDR) | $(OBJ)
	$(create_obj)

$(OBJ)$(L_LIST_TEST_OBJ): $(L_LIST_TEST) $(L_LIST_HDR) | $(OBJ)
	$(create_obj)

# Bolsa de letras
$(L_BAG_EXEC): $(L_BAG_TEST_OBJ) $(L_BAG_OBJ) $(L_LIST_OBJ) $(LETTER_OBJ) $(DIC_OBJ)
	$(create_bin)

$(OBJ)$(L_BAG_OBJ): $(L_BAG_SRC) $(L_BAG_HDR) $(L_LIST_HDR) $(LETTER_HDR) | $(OBJ)
	$(create_obj)

$(OBJ)$(L_BAG_TEST_OBJ): $(L_BAG_TEST) $(L_BAG_HDR) | $(OBJ)
	$(create_obj)

# Inteligencia Artificial
$(IA_EXEC): $(IA_TEST_OBJ) $(IA_OBJ) $(LETTER_OBJ) $(DIC_OBJ)
	$(create_bin)

$(OBJ)$(IA_OBJ): $(IA_SRC) $(IA_HDR) $(LETTER_HDR) $(DIC_HDR) | $(OBJ)
	$(create_obj)

$(OBJ)$(IA_TEST_OBJ): $(IA_TEST) $(IA_HDR) | $(OBJ)
	$(create_obj)

# Cantidad de letras
$(L_FREC_EXEC): $(L_FREC_OBJ) $(DIC_OBJ)
	$(create_bin)

$(OBJ)$(L_FREC_OBJ): $(L_FREC_SRC) $(DIC_OBJ) | $(OBJ)
	$(create_obj)

# Juego
$(GAME_EXEC): $(MAIN_GAME_OBJ) $(GAME_OBJ) $(DIC_OBJ) $(LETTER_OBJ) $(L_LIST_OBJ) $(L_BAG_OBJ) $(IA_OBJ)
	$(create_bin)

$(GAME_OBJ): %.o: %.cpp %.h $(DIC_HDR) $(LETTER_HDR) $(L_LIST_HDR) $(L_BAG_HDR) $(IA_HDR) | $(OBJ)
	$(create_obj)

$(MAIN_GAME_OBJ): $(MAIN_GAME_SRC) $(GAME_HDR) $(DIC_HDR) $(LETTER_HDR) $(L_LIST_HDR) $(L_BAG_HDR) $(IA_HDR) | $(OBJ)
	$(create_obj)

# Generales
$(BIN):
	$(make_dir)

$(OBJ):
	$(make_dir)

$(TESTS):
	$(make_dir)

clean:
	@echo "Borrando ejecutable, objetos y tests..."
	@-rm -rf $(BIN) $(OBJ) $(TESTS)
	@echo "${GREEN}Hecho${RESET}"
	@echo "Borrando documentación..."
	@-rm -rf $(DOC)/html $(DOC)/latex
	@echo "${GREEN}Hecho${RESET}"

dic_test: $(DIC_EXEC)
	$(perform_test)

letter_test: $(LETTER_EXEC)
	$(perform_test)

letter_list_test: $(L_LIST_EXEC)
	$(perform_test)

letter_bag_test: $(L_BAG_EXEC)
	$(perform_test)

ia_test: $(IA_EXEC)
	$(perform_test)

l_frec_test: $(L_FREC_EXEC)
	$(perform_test)

$(DOCUMENTATION):
	@-rm -rf $(DOC)/html $(DOC)/latex
	@$(DOCUMENTATION) $(DOC)pfinal.doxy
	@$(BROWSER) $(DOC)/html/index.html &

help:
	@echo "${BOLD}###################################################################################${RESET}"
	@echo "LISTA DE OPCIONES:"
	@echo " 	help			Muestra este mensaje de ayuda"
	@echo " 	author			Muestra información sobre los autores"
	@echo " 	dic			Crea el ejecutable diccionario"
	@echo " 	letter			Crea el ejecutable letra"
	@echo " 	letter_list		Crea el ejecutable lista_letras"
	@echo " 	letter_bag		Crea el ejecutable bolsa_letras"
	@echo " 	ia_project		Crea el ejecutable ia"
	@echo " 	l_frec			Crea el ejecutable cantidad_letras"
	@echo " 	[program]_test		Realiza el test del programa \"program\""
	@echo " 	clean			Elimina los archivos objeto y ejecutables"
	@echo " 	doxygen BROWSER=	Crea la documentación y la ejecuta en BROWSER"
	@echo "${BOLD}###################################################################################${RESET}"

author:
	@echo "Este proyecto ha sido realizado por:"
	@echo "-Manuel Gachs Ballegeer: \
	https://gitlab.com/Manuelbelgicano"
	@echo "-Gonzalo Moreno Soto: \
	https://gitlab.com/delightfulagony"
	@echo "Para la asignatura de Estructura de Datos de la UGR"
