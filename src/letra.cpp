/**
 * @file letra.cpp
 * @authors Manuel Gachs Ballegeer
 * 		    Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<cctype>
#include"letra.h"

void letra::scrabble() {
	if (c=='Z')
		p = 10;
	else if (c=='K' || c=='Q' || c=='W' || c=='X')
		p = 8;
	else if (c=='J')
		p = 6;
	else if (c=='F' || c=='H' || c=='V' || c=='Y')
		p = 4;
	else if (c=='M' || c=='B' || c=='P')
		p = 3;
	else if (c=='C' || c=='D' || c=='G')
		p = 2;
	else
		p = 1;
}

letra::letra() {
	c = 'A';
	p = 0;
}

letra::letra(const letra &orig) {
	c = toupper(orig.c);
	p = orig.p;
}

letra::letra(const char &_c,const unsigned int &_p) {
	c = toupper(_c);
	p = _p;
}

letra::letra(const char &_c) {
	c = toupper(_c);
	scrabble();
}

void letra::setChar(const char &_c) {
	c = toupper(_c);
	scrabble();
}

letra& letra::operator=(const letra &l) {
	c = toupper(l.c);
	p = l.p;
	return *this;
}

bool letra::operator==(const letra &l) const {
	if (c==l.c && p==l.p)
		return true;
	else
		return false;
}
