/**
 * @file ia_test.cpp
 * @brief Pequeño programa para testear la clase arbol_ia
 * @authors	Manuel Gachs Ballegeer
 * 			Gonzalo Moreno Soto
 * @license GPLv3
 */

#include <iostream>
#include "ia.h"

int main(const int argc,const char* argv[]) {
	if (argc<2) {
		std::cout<<"TEST FALLIDO: Faltan argumentos\n";
		return -1;
	}
	diccionario aux, dic;
	for (int i=1;i<argc;i++) {
		aux = fromFile(argv[i]);
		std::cout<<std::endl<<i<<"º diccionario ("<<aux.size()<<" palabras)\n\n";
		std::cout<<aux<<std::endl;
		dic += aux;
		std::cout<<"El diccionario total tiene "<<dic.size()<<" palabras\n";
	}
	arbol_ia arbol(dic);
	return 0;
}
