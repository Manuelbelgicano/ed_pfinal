/**
 * @file letra_test.cpp
 * @authors Manuel Gachs Ballegeer
 * 		    Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<iostream>
#include"letra.h"
/**
 * @brief Escribe los datos de una letra en pantalla
 * @param a Letra que se quiere escribir
 */
void print(const letra &a);

int main() {
	char aux_char;
	unsigned int aux_value;
	letra a('a',1);
	std::cout<<"\nMostrando primera letra:\n";
	print(a);

	std::cout<<"Introduce los datos para una letra: \n";
	std::cout<<"Formato: caracter valor\n";
	std::cin>>aux_char>>aux_value;
	letra aux(aux_char,aux_value);
	std::cout<<"\nMostrando la segunda letra\n";
	print(aux);
	
	if (a==aux)
		std::cout<<"Las dos letras son iguales\n";
	else
		std::cout<<"Las letras son distintas\n";
	
	return 0;
}

void print(const letra &a) {
	std::cout<<"Letra "<<a.getChar()<<":\n";
	std::cout<<"Puntuación: "<<a.getValue()<<std::endl;
}
