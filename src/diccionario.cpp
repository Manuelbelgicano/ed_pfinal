/**
 * @file diccionario.cpp
 * @authors Manuel Gachs Ballegeer
 * 		    Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<fstream>
#include<utility>
#include"diccionario.h"

diccionario& diccionario::operator+=(const diccionario &d) {
	diccionario::const_iterator it1 = d.cbegin();
	diccionario::const_iterator it2 = d.cend();
	
	dic.insert(it1,it2);
	return *this;
}

void diccionario::getFrecuencias(const std::set<char> letras, const char file[]) const {
	double fabs[letras.size()] = {0};
	double total;
	size_t posicion = 0;
	std::set<char>::const_iterator it1;
	diccionario::const_iterator it2;

	for (it2=cbegin();it2!=cend();it2++)
		for (size_t i=0;i<(*it2).size();i++) {
			posicion = 0;
			for (it1=letras.cbegin();it1!=letras.cend();it1++) {
				if (toupper((*it2)[i])==toupper((*it1))) {
					fabs[posicion]++;
					total++;
				}
				posicion++;
			}
		}
	
	std::ofstream fout;
	fout.open(file);
	if (!fout) {
		std::cout<<"Error abriendo el archivo "<<file<<std::endl;
		return;
	}
	posicion = 0;
	fout<<"LETRA\tFAbs\tFRel\n";
	for (it1=letras.cbegin();it1!=letras.cend();it1++) {
		fout<<(*it1)<<"\t"<<fabs[posicion]<<"\t"<<fabs[posicion]/total<<std::endl;
		posicion++;
	}
	fout.close();
	return;
}

std::istream& operator>>(std::istream &is,diccionario &d) {
	std::string palabra;
	while (is) {
		is>>palabra;
		d.insert(palabra);
	}
	return is;
}

std::ostream& operator<<(std::ostream &os,const diccionario &d) {
	diccionario::const_iterator it;

	for (it=d.cbegin();it!=d.cend();++it)
		os<<*it<<std::endl;
	return os;
}

diccionario fromFile(const char file[]) {
	std::ifstream fin;
	diccionario aux;
	fin.open(file);
	if (!fin) {
		std::cout<<"Error abriendo el archivo\n";
		return aux;
	}

	fin>>aux;
	fin.close();
	return aux;
}

bool toFile(const char file[],const diccionario &d) {
	std::ofstream fout;
	fout.open(file);
	if (!fout) {
		std::cout<<"Error abriendo el archivo\n";
		return false;
	}

	fout<<d;
	fout.close();
	return true;
}
