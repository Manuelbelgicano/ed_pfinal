/**
 * @file letras.cpp
 * @authors Manuel Gachs Ballegeer
 * 	    Gonzalo Moreno Soto
 */

#include<cstdlib>
#include<cstring>
#include<iostream>
#include<set>
#include<time.h>
#include<vector>
#include"auxiliares.h"
#include"bolsa_letras.h"
#include"buscador_arbol.h"
#include"diccionario.h"
#include"ia.h"
#include"lista_letras.h"

int main(const int argc,const char* argv[]) {
	bool is_longitud = false;
	size_t num_letras = 8;

	// Casulística con los argumentos
	if ( argc>2 && argc<=4) {
		if (argc==3) {
			if (0==strcmp(argv[2],"L"))
				is_longitud = true;
			else if (0!=strcmp(argv[2],"P"))
				num_letras = atoi(argv[2]);
		} else {
			if (0!=strcmp(argv[2],"L") && 0!=strcmp(argv[2],"P"))
				num_letras = atoi(argv[2]);
			else {
				std::cout<<"Error en los argumentos\n";
				help();
				return -1;
			}
			if (0==strcmp(argv[3],"L"))
				is_longitud = true;
			else if (0!=strcmp(argv[3],"P")) {
				std::cout<<"Error en los argumentos\n";
				help();
				return -1;
			}
		}
	} else {
		std::cout<<"Error en el número de argumentos\n";
		help();
		return -1;
	}

	// Generación del árbol, el diccionario y la bolsa de letras
	std::cout<<"Generando diccionario\n";
	diccionario dic = fromFile(argv[1]);
	std::cout<<"HECHO\nGenerando bolsa de letras\n";
	lista_letras l_aux = fromDic(dic);
	bolsa_letras conjunto_letras(l_aux);
	std::cout<<"HECHO\nGenerando árbol\n";
	arbol_ia arbol_palabras(dic);
	std::cout<<"HECHO\n";

	//Generación de los materiales del juego
	srand(time(NULL));
	if (is_longitud) {
		std::cout<<"Cambiando la puntuación a longitud\n";
		arbol_palabras.puntuarLongitud();
		std::cout<<"HECHO\n";
	}

	// Desarrollo del juego
	std::string palabra_jugador;
	char seguir_jugando = 'S';
	std::vector<solucion> soluciones;
	std::multiset<letra,classcomp_letra> letras_juego;
	while (seguir_jugando=='S') {
		// Generación del conjunto de letras aleatorio
		std::cout<<std::endl<<std::endl;
		letras_juego = getLetrasJuego(conjunto_letras,num_letras);
		printLetrasJuego(letras_juego);

		// Solución de la IA
		soluciones = getSolucionesIA(letras_juego,arbol_palabras);

		// Solución del jugador
		std::cout<<"Tu palabra: ";
		std::cin>>palabra_jugador;
		soluciones.push_back(getSolucion(palabra_jugador,letras_juego,dic,is_longitud));

		// Resultado de la partida
		printSoluciones(soluciones);

		// Volver a jugar
		std::cout<<"¿Quieres volver a jugar?[S/N]: ";
		std::cin>>seguir_jugando;
	}
	return 0;
}
