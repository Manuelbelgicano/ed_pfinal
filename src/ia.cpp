#include "ia.h"

bool esNodoMayor(nodo_ia a, nodo_ia b) {
	return (a.mejor>b.mejor);
}

nodo_ia::nodo_ia() {
	nivel=0;
	padre=nullptr;
}

nodo_ia::nodo_ia(const letra _id, nodo_ia* _padre) {
	id=_id;
	padre=_padre;
	nivel=((*padre).nivel)+1;
	mejor=((*padre).mejor)+id.getValue();
}

void nodo_ia::addNodo(letra _letra) {
	nodo_ia naux(_letra, this);
	hijos.push_back(naux);
}

arbol_ia::arbol_ia(diccionario dic) {
	diccionario::iterator inicio;
	diccionario::iterator fin;
	inicio=dic.begin();
	fin=dic.end();

	for(diccionario::iterator it=inicio;it!=fin;it++)
		addPalabra(*it);

	ordenar();
}

nodo_ia* arbol_ia::getRaiz() {
	return &raiz;
}

void arbol_ia::addPalabra(std::string palabra) {
	bool existeLetra, finBucle=false;
	arbol_ia_iterator it(*this);
	it++;
	letra lAux;
	nodo_ia nAux;

	while(!finBucle) {
		existeLetra=false;
		if(((*it)->nivel)==(palabra.size())) {
			finBucle=true;
			break;
		}
		if((*it)->hijos.empty()) {
			lAux.setChar(palabra[(*it)->nivel]);
			(*it)->addNodo(lAux);
			it++;
		} else if((*it)->id.getChar()==palabra[(*it)->nivel]) {
			it++;
		} else {
			it.apuntaPadre();
			for(size_t i=0;i<(*it)->hijos.size();i++) {
				if(((*it)->hijos[i].id.getChar())==(palabra[(*it)->nivel])) {
					it.nodo=&((*it)->hijos[i]);
					it.posicionVertical.push(i);
					existeLetra=true;
					it++;
					break;
				}
			}
			if(!existeLetra) {
				lAux.setChar(palabra[((*it)->nivel)+1]);
				(*it)->addNodo(lAux);
			}
		}
	}
}

void arbol_ia::ordenar() {
	arbol_ia_iterator it(*this);
	std::sort ((*it)->hijos.begin(), (*it)->hijos.end(), esNodoMayor);
	while(it.nodo!=nullptr) {
		if(!(*it)->hijos.empty()) {
			std::sort ((*it)->hijos.begin(), (*it)->hijos.end(), esNodoMayor);
			it++;
		} else {
			it--;
		}
	}
}

arbol_ia_iterator::arbol_ia_iterator(arbol_ia arbol) {
	nodo=arbol.getRaiz();
}

arbol_ia_iterator& arbol_ia_iterator::operator++(int x) {
	nodo=&((*nodo).hijos[1]);
	posicionVertical.push(1);
	return *this;
}

arbol_ia_iterator& arbol_ia_iterator::operator--(int x) {
	unsigned int aux;
	posicionVertical.pop();
	nodo=(*nodo).padre;
	aux=posicionVertical.top();
	aux++;
	posicionVertical.pop();
	posicionVertical.push(aux);
	if((*nodo).hijos.size()<=posicionVertical.top()) {
		if((*nodo).padre!=nullptr) {
			nodo=(*nodo).padre;
			posicionVertical.pop();
		} else 
			nodo=nullptr;
	} else {
		nodo=&((*nodo).hijos[posicionVertical.top()]);
	}
	return *this;
}

arbol_ia_iterator& arbol_ia_iterator::apuntaPadre() {
	nodo=(*nodo).padre;
	posicionVertical.pop();
	return *this;
}

nodo_ia* arbol_ia_iterator::operator*() {
	return nodo;
}
