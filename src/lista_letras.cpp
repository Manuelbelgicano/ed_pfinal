/**
 * @file lista_letras.cpp
 * @authors Manuel Gachs Ballegeer
 * 		    Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<fstream>
#include"letra.h"
#include"lista_letras.h"

datos_letra datos_letra::operator=(const datos_letra &a) {
	datos_letra aux;
	aux.c = a.c;
	aux.l = a.l;
	return aux;
}

datos_letra operator+(const datos_letra &a,const datos_letra &b) {
	datos_letra aux;
	if (a.l==b.l) {
		aux.l = a.l;
		aux.c = a.c + b.c;
	}
	return a;
}

lista_letras::lista_letras(const lista_letras &orig) {
	datos = orig.datos;
}

lista_letras& lista_letras::operator=(const lista_letras &ll) {
	datos = ll.datos;
	return *this;
}

bool lista_letras::isLetter(const char _l,lista_letras::iterator &it) {
	for (it=begin();it!=end();it++)
		if ((*it).l.getChar()==_l)
			return true;
	it = end();
	return false;
}

void lista_letras::insertLetter(const letra &_l,unsigned int _c) {
	datos_letra aux;
	aux.l = _l;
	aux.c = _c;
	lista_letras::iterator it;
	
	if (isLetter(_l.getChar(),it)) {
		aux.c += (*it).c;
		datos.erase(it);
	}
	datos.insert(aux);
	return;
}

lista_letras& lista_letras::operator+(const lista_letras &ll) {
	lista_letras::const_iterator it;

	for (it=ll.cbegin();it!=ll.cend();it++)
		insertLetter((*it).l,(*it).c);
	return *this;
}

bool lista_letras::toFile(const char file[]) {
	std::ofstream fout;
	fout.open(file);
	if (!fout)
		return false;
	
	fout<<*this;
	fout.close();
	return true;
}

std::ostream& operator<<(std::ostream &os,const lista_letras &ll) {
	lista_letras::const_iterator it;

	os<<"LETRA\tCANTIDAD\tVALOR\n";
	for (it=ll.cbegin();it!=ll.cend();it++) {
		os<<(*it).l.getChar()<<"\t"<<(*it).c<<"\t\t"<<(*it).l.getValue();
		os<<std::endl;
	}
	os<<std::endl;
	return os;
}

lista_letras fromString(const std::string &s) {
	lista_letras aux;
	letra l_aux;

	for (size_t i=0;i<s.size();i++) {
		l_aux.setChar(s[i]);
		if (l_aux.getChar()!=' ')
			aux.insertLetter(l_aux,1);
	}
	return aux;
}

lista_letras fromDic(const diccionario &dic) {
	lista_letras aux;
	std::string s_aux;
	diccionario::const_iterator it;

	for (it=dic.cbegin();it!=dic.cend();it++)
		aux = aux + fromString((*it));

	return aux;
}

lista_letras fromDicFile(const char file[]) {
	diccionario d_aux = fromFile(file);
	lista_letras aux = fromDic(d_aux);
	return aux;
}
