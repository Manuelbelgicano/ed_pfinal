/**
 * @file bolsa_letras_test.cpp
 * @authors Manuel Gachs Ballegeer
 *			Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<cstdlib>
#include<iostream>
#include<set>
#include<time.h>
#include"bolsa_letras.h"

/**
 * @brief Imprime un conjunto de letras en la consola
 * @param m Conjunto de letras a imprimir
 */
void print(const std::multiset<letra,classcomp_letra> &m);

int main(const int argc,const char* argv[]) {
	if (argc<2) {
		std::cout<<"TEST FALLIDO: Faltan argumentos\n";
		return -1;
	}
	
	std::cout<<"La bolsa se creará a partir del archivo "<<argv[1]<<std::endl;
	srand(time(0));
	lista_letras aux = fromDicFile(argv[1]);
	bolsa_letras conjunto(aux);
	std::multiset<letra,classcomp_letra> salida;

	std::cout<<"Sacamos 8 letras dos veces:\n";
	std::cout<<"PRIMERA VEZ: ";
	salida = conjunto.sample(8);
	print(salida);
	std::cout<<"SEGUNDA VEZ: ";
	salida = conjunto.sample(8);
	print(salida);
}

void print(const std::multiset<letra,classcomp_letra> &m) {
	std::multiset<letra>::const_iterator it;
	for (it=m.cbegin();it!=m.cend();it++)
		std::cout<<(*it).getChar()<<" ";
	std::cout<<std::endl;
}
