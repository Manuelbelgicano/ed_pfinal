/**
 * @file buscador_arbol.cpp
 * @authors Manuel Gachs Ballegeer
 * 	    Gonzalo Moreno Soto
 */

#include<set>
#include<string>
#include"auxiliares.h"
#include"buscador_arbol.h"
#include"diccionario.h"
#include"ia.h"
#include"letra.h"

std::vector<solucion> getSolucionesIA(const std::multiset<letra,classcomp_letra> &letras,arbol_ia &arbol) {
	std::multiset<letra,classcomp_letra> conjunto_letras = letras;
	std::vector<solucion> sols;
	arbol_ia_iterator it(arbol);
	std::string palabra_aux;
	solucion sol_aux;
	size_t nivel = 0;

	it++;
	while (*it!=nullptr) {
		// Estamos en una hoja del árbol
		if ((*it)->hijos.empty()) {
			arreglarString(nivel,(*it)->nivel,palabra_aux,conjunto_letras);
			if (!sols.empty() && sols[sols.size()-1].puntuacion>puntosNodo(*it))
				return sols;
			else {
				if(isLetra((*it)->id.getChar(),conjunto_letras)) {
					palabra_aux.push_back((*it)->id.getChar());
					sol_aux.palabra = palabra_aux;
					sol_aux.puntuacion = puntosNodo(*it);
					sols.push_back(sol_aux);
				}
				it--;
			}
		// No estamos en una hoja del árbol
		} else {
			arreglarString(nivel,(*it)->nivel,palabra_aux,conjunto_letras);
			if (!sols.empty() && sols[sols.size()-1].puntuacion>puntosNodo(*it))
				return sols;
			else {
				if(isLetra((*it)->id.getChar(),conjunto_letras)) {
					palabra_aux.push_back((*it)->id.getChar());
					if (conjunto_letras.empty()) {
						sol_aux.palabra = palabra_aux;
						sol_aux.puntuacion = puntosNodo(*it);
						sols.push_back(sol_aux);
						it--;
					} else
						it++;
				} else
					it--;
			}
		}
	}
	return sols;
}

bool isLetra(const char &_letra,std::multiset<letra,classcomp_letra> &conjunto) {
	std::multiset<letra,classcomp_letra>::iterator it;

	if (conjunto.empty())
		return false;
	else {
		for (it=conjunto.begin();it!=conjunto.end();it++)
			if ((*it).getChar()==_letra) {
				conjunto.erase(it);
				return true;
			}
		return false;
	}
}

void arreglarString(size_t &prenivel,const size_t &nuevonivel,std::string &palabra,std::multiset<letra,classcomp_letra> &conjunto) {
	letra aux;
	if (palabra.empty()) {
		prenivel = nuevonivel;
		return;
	} else if (prenivel==nuevonivel) {
		if (prenivel==palabra.size()) {
			aux.setChar(palabra.back());
			conjunto.insert(aux);
			palabra.pop_back();
			prenivel = nuevonivel;
			return;
		}
	} else if (prenivel>nuevonivel) {
		while (palabra.size()>=nuevonivel) {
			aux.setChar(palabra.back());
			conjunto.insert(aux);
			palabra.pop_back();
		}
		prenivel = nuevonivel;
		return;
	}
}

size_t puntosNodo(const nodo_ia* nodo) {
	size_t puntos = nodo->mejor;
	nodo_ia* papa = nodo->padre;

	while (papa->nivel>=1) {
		puntos += papa->id.getValue();
		papa = papa->padre;
	}
	return puntos;
}
