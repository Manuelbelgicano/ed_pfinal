/**
 * @file lista_letras_test.cpp
 * @authors Manuel Gachs Ballegeeer
 *			Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<iostream>
#include"lista_letras.h"

int main(const int argc,const char* argv[]) {
	if (argc<3) {
		std::cout<<"TEST FALLIDO: Error en el número de argumentos\n";
		return -1;
	}
	lista_letras lista1;
	lista_letras lista2;
	lista_letras lista3;
	std::string palabra = "Esternocleidomastoideo";
	diccionario dic = fromFile(argv[1]);

	lista1 = fromString(palabra);
	std::cout<<"\nPrimera lista de letras formada a partir de:\n";
	std::cout<<palabra<<std::endl<<std::endl;
	std::cout<<lista1;

	lista2 = fromDic(dic);
	std::cout<<"\nSegunda lista de letras formada a partir de diccionario\n";
	std::cout<<lista2;

	lista3 = fromDicFile(argv[2]);
	std::cout<<"\nTercera lista de letras formada a partir de archivo\n";
	std::cout<<lista3;

	std::cout<<"\nSumando las tres listas\n";
	lista_letras suma = lista1 + lista2 + lista3;
	std::cout<<suma;
	lista_letras::iterator aux;
	if (suma.isLetter('Q',aux))
		std::cout<<"La lista contiene la Q\n";

	std::cout<<"Se guardará en test/Scrabble_test.txt\n";
	suma.toFile("test/Scrabble_test.txt");
	return 0;
}
