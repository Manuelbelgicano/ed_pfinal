/**
 * @file bolsa_letras.cpp
 * @authors Manuel Gachs Ballegeer
 * 		    Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<algorithm>
#include<cstdlib>
#include<random>
#include"bolsa_letras.h"

void bolsa_letras::shuffle() {
	std::shuffle(bolsa.begin(),bolsa.end(),std::default_random_engine());
}

bolsa_letras::bolsa_letras(const bolsa_letras &orig) {
	bolsa = orig.bolsa;
	shuffle();
}

bolsa_letras::bolsa_letras(const lista_letras &ll) {
	lista_letras::const_iterator it;
	for (it=ll.cbegin();it!=ll.cend();it++)
		for (size_t i=0;i<(*it).c;i++)
			bolsa.push_back((*it).l);
	shuffle();
}

std::multiset<letra,classcomp_letra> bolsa_letras::sample(const size_t &n) const {
	size_t counter = 0;
	std::multiset<letra,classcomp_letra> aux;

	while (counter<n) {
		aux.insert(bolsa[rand() % bolsa.size()]);
		counter++;
	}
	return aux;
}
