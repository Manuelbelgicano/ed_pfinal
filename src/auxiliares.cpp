/**
 * @file auxiliares.cpp
 * @authors Manuel Gachs Ballegeer
 *	    Gonzalo Moreno Soto
 */

#include<iostream>
#include<set>
#include<stack>
#include<string>
#include<vector>
#include"auxiliares.h"
#include"diccionario.h"
#include"bolsa_letras.h"
#include"ia.h"
#include"letra.h"

solucion& solucion::operator=(const solucion &sol) {
	palabra = sol.palabra;
	puntuacion = sol.puntuacion;
	return *this;
}

solucion getSolucion(const std::string &_palabra,std::multiset<letra,classcomp_letra> &conjunto_letras,const diccionario &dic,bool longitud) {
	solucion aux;
	aux.palabra = _palabra;
	if (!isPalabra(_palabra,dic)) {
		aux.puntuacion = -1;
		return aux;
	} else if (!isPossible(_palabra,conjunto_letras)) {
		aux.puntuacion = -1;
		return aux;
	} else
		aux.puntuacion = getPuntuacion(_palabra,longitud);
	return aux;
}

void printSoluciones(const std::vector<solucion> &sols) {
	solucion mejor = sols[0];

	for (size_t i=0;i<sols.size();i++) {
		if (sols[i].puntuacion!=-1) {
			if (i==sols.size()-1) {
				std::cout<<"Tu solución: "<<sols[i].palabra<<std::endl;
				std::cout<<"Puntuación: "<<sols[i].puntuacion<<std::endl;
			} else {
				std::cout<<"Solución: "<<sols[i].palabra<<std::endl;
				std::cout<<"Puntuación: "<<sols[i].puntuacion<<std::endl;
			}
		} else
			std::cout<<"Tu solución no es válida\n";
		if (sols[i].puntuacion>mejor.puntuacion)
			mejor = sols[i];
	}

	std::cout<<"La mejor solución es: "<<mejor.palabra<<std::endl;
	std::cout<<"Con la puntuación: "<<mejor.puntuacion<<std::endl;

	return;
}

void help() {
	std::cout<<"EJECUCIÓN DEL PROGGRAMA LETRAS:\n";
	std::cout<<"./letras archivo.txt [num_letras modo_juego]\n";
	std::cout<<"archivo.txt --> archivo del diccionario\n";
	std::cout<<"num_letras  --> número de letras con el que se quiere jugar\n";
	std::cout<<"		    (por defecto 8)\n";
	std::cout<<"modo_juego  --> 'P' si se quiere jugar con la puntuación de las letras\n";
	std::cout<<"		    'L' si se quiere jugar con la longitud de las palabras\n";
	std::cout<<"		    (por defecto 'P')\n";
	return;
}

std::multiset<letra,classcomp_letra> getLetrasJuego(const bolsa_letras &bl,const size_t &n){
	std::multiset<letra,classcomp_letra> aux;
	std::cout<<"Generando conjunto de letras aleatorio\n";
	aux = bl.sample(n);
	std::cout<<"HECHO\n";
	return aux;
}

void printLetrasJuego(const std::multiset<letra,classcomp_letra> &lj){
	std::multiset<letra,classcomp_letra>::const_iterator it;

	std::cout<<"CONJUNTO DE LETRAS:\n";
	for (it=lj.cbegin();it!=lj.cend();it++)
		std::cout<<(*it).getChar()<<" ";
	std::cout<<std::endl;
	return;
}

bool isPalabra(const std::string &palabra,const diccionario &dic) {
	diccionario::const_iterator it;

	for (it=dic.cbegin();it!=dic.cend();it++)
		if ((*it)==palabra)
			return true;
	return false;
}

bool isPossible(const std::string &palabra,std::multiset<letra,classcomp_letra> &conjunto_letras) {
	std::multiset<letra,classcomp_letra>::iterator it = conjunto_letras.begin();
	size_t numero_letras = 0;
	bool is_char = false;

	for (size_t i=0;i<palabra.size();i++) {
		is_char = false;
		for (it=conjunto_letras.begin();it!=conjunto_letras.end() && !is_char;it++) {
			if (palabra[i]==(*it).getChar()) {
				numero_letras++;
				conjunto_letras.erase(it);
				is_char = true;
			}
		}
	}

	if (numero_letras==palabra.size())
		return true;
	else
		return false;
}

size_t getPuntuacion(const std::string &palabra,bool equal) {
	size_t aux = 0;
	letra l_aux;

	if (equal)
		aux = palabra.size();
	else
		for (size_t i=0;i<palabra.size();i++) {
			l_aux.setChar(palabra[i]);
			aux += l_aux.getValue();
		}
	return aux;
}

