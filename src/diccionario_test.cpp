/**
 * @file diccionario_test.cpp
 * @authors Manuel Gachs Ballegeer
 *	 	    Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<iostream>
#include"diccionario.h"

int main(const int argc,const char* argv[]) {
	if (argc<2) {
		std::cout<<"TEST FALLIDO: Faltan argumentos\n";
		return -1;
	}
	diccionario aux, dic;
	for (int i=1;i<argc;i++) {
		aux = fromFile(argv[i]);
		std::cout<<std::endl<<i<<"º diccionario ("<<aux.size()<<" palabras)\n\n";
		std::cout<<aux<<std::endl;
		dic += aux;
		std::cout<<"El diccionario total tiene "<<dic.size()<<" palabras\n";
	}

	std::cout<<"Guardando el diccionario en test/dic_test.txt\n";
	if (!toFile("test/dic_test.txt",dic))
		std::cout<<"Error al guardar el diccionario\n";
	return 0;
}
