/**
 * @file cantidad_letras.cpp
 * @authors Manuel Gachs Ballegeer
 * 	    Gonzalo Moreno Soto
 */

#include<fstream>
#include<iostream>
#include<set>
#include<utility>
#include"diccionario.h"

/**
 * @brief Crea un conjunto de letras a partir de un archivo
 * @param file Fichero con las letras
 * @return Un conjunto de letras
 * @post En caso de error, devuelve un conjunto vacío
 */
std::set<char> getLetras(const char file[]);
/**
 * @brief Muestra un mensaje de ayuda en caso de error
 */
void help();

int main(const int argc,const char* argv[]) {
	if (argc!=4) {
		std::cout<<"Error en el número de argumentos\n";
		help();
		return -1;
	}

	std::cout<<"Generando el diccionario\n";
	diccionario dic = fromFile(argv[1]);
	std::cout<<"Generando el conjunto de letras\n";
	std::set<char> letras = getLetras(argv[2]);
	std::cout<<"Generando el archivo de salida\n";
	dic.getFrecuencias(letras,argv[3]);
	return 0;
}

std::set<char> getLetras(const char file[]) {
	std::ifstream fin;
	std::set<char> aux;
	char c_aux;

	fin.open(file);
	if (!fin)
		std::cout<<"Error al abrir el archivo "<<file<<std::endl;
	else
		while(fin) {
			fin>>c_aux;
			aux.insert(toupper(c_aux));
		}
	fin.close();
	return aux;
}

void help() {
	std::cout<<"Formato de ejecución:\n";
	std::cout<<"./cantidad_letras <archivo1> <archivo2> <archivo3>\n";
	std::cout<<"archivo1:	archivo diccionario\n";
	std::cout<<"archivo2:	archivo de letras\n";
	std::cout<<"achivo3:	archivo de salida\n";
	return;
}
