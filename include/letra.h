/**
 * @file letra.h
 * @authors Manuel Gachs Ballegeer
 * 		    Gonzalo Moreno Soto
 * @license GPLv3
 */

#ifndef _LETRA_H
#define _LETRA_H

/**
 * @brief TDA letra. Representa a una letra con su puntuación asociada
 */
class letra {
private:
	char c; 		///< Caracter de la letra
	unsigned int p; 	///< Puntuación de la letra
	void scrabble();		///< Le da la puntuación del Scrabble a una letra
public:
	/**
	 * @brief Constructor por defecto
	 */
	letra();
	/**
	 * @brief Constructor por copia
	 * @param orig Letra que se copia
	 */
	letra(const letra &orig);
	/**
	 * @brief Constructor primitivo
	 * @param _c Caracter de la letra
	 * @param _p Puntuación de la letra
	 */
	letra(const char &_c,const unsigned int &_p);
	/**
	 * @brief Constructor primitivo.
	 * 	  Puntúa la letra de acuerdo con el Scrabble
	 * @param _c Caracter de la letra
	 */
	letra(const char &_c);
	/**
	 * @brief Cambia el caracter de una letra dada.
	 * @param _c El caracter a asignar
	 */
	void setChar(const char &_c);
	/**
	 * @brief Consulta el caracter de la letra
	 * @return El caracter de la letra. Puede ser modificado
	 */
	char& getChar() {return c;}
	/**
	 * @brief Consulta el caracter de la letra
	 * @return El caracter de la letra
	 */
	char getChar() const {return c;}
	/**
	 * @brief Consulta la puntuación de una letra
	 * @return La puntuación de la letra. Puede ser modificado
	 */
	unsigned int& getValue() {return p;}
	/**
	 * @brief Consulta la puntuación de una letra
	 * @return La puntuación de la letra
	 */
	unsigned int getValue() const {return p;}
	/**
	 * @brief Sobrecarga del operador de asignación
	 * @param l Letra que se asigna
	 * @return La letra asignada
	 */
	letra& operator=(const letra &l);
	/**
	 * @brief Determina si dos letras son iguales.
	 * @param l Letra con la se compara
	 * @return @retval true Si son iguales
	 * 	   @retval false En caso contrario
	 */
	bool operator==(const letra &l) const;
};

#endif
