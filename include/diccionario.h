/**
 * @file diccionario.h
 * @authors Manuel Gachs Ballegeer
 * 		    Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<iostream>
#include<set>
#include<string>

#ifndef _DICCIONARIO_H
#define _DICCIONARIO_H

/**
 * @brief TDA diccionario
 */
class diccionario {
private:
	std::set<std::string> dic; ///< Conjunto donde se almacenan las palabras
public:
	/**
	 * @brief Constructor por defecto
	 */
	diccionario() {}
	/**
	 * @brief Consulta el tamaño del diccionario
	 * @return El tamaño del diccionario
	 */
	size_t size() const {return dic.size();}
	/**
	 * @brief Inserta una palabra en el diccionario
	 * @param palabra Palabra que se quiere añadir
	 */
	void insert(const std::string &palabra) {dic.insert(palabra);}
	/**
	 * @brief Añade un diccionario a otro
	 * @param d Diccionario que se añade
	 * @return La suma de los dos diccionarios
	 */
	diccionario& operator+=(const diccionario &d);
	/**
	 * @brief Obtiene las frecuencias absolutas y relativas de un conjunto de letras
	 * @param letras Conjunto de letras
	 * @param file Fichero donde se guarda la información
	 */
	void getFrecuencias(const std::set<char> letras,const char file[]) const;
	/**
	 * @brief Clase de los iteradores de diccionario
	 */
	typedef std::set<std::string>::iterator iterator;
	/**
	 * @brief Iterador al inicio del diccionario
	 * @return Un iterador al inicio del diccionario
	 */
	iterator begin() {return dic.begin();}
	/**
	 * @brief Iterador al final del diccionario
	 * @return Un iterador al final del diccionario
	 */
	iterator end() {return dic.end();}
	/**
	 * @brief Clase de los iteradores constantes de diccionario
	 */
	typedef std::set<std::string>::const_iterator const_iterator;
	/**
	 * @brief Iterador constante al inicio del diccionario
	 * @return Un iterador constante al inicio del diccionario
	 */
	const_iterator cbegin() const {return dic.cbegin();}
	/**
	 * @brief Iterador constante al final del diccionario
	 * @return Un iterador constante al final del diccionario
	 */
	const_iterator cend() const {return dic.cend();}
	/**
	 * @brief Sobrecarga del operador de entrada
	 */
	friend std::istream& operator>>(std::istream &is,diccionario &d);
	/**
	 * @brief Sobrecarga del operador de salida
	 */
	friend std::ostream& operator<<(std::ostream &os,const diccionario &d);
};

std::istream& operator>>(std::istream &is,diccionario &d);
std::ostream& operator<<(std::ostream &os,const diccionario &d);
/**
 * @brief Crea un diccionario a partir de un archivo
 * @param file Nombre del archivo
 * @return Un diccionario a partir del archivo
 */
diccionario fromFile(const char file[]);
/**
 * @brief Guarda un diccionario en un fichero
 * @param file Nombre del fichero
 * @param d Diccionario que se va a guardar
 * @return @retval true Si ha realizado la operación con éxito
 * 	   @retval false En caso contrario
 */
bool toFile(const char file[],const diccionario &d);

#endif
