/**
 * @file buscador_arbol.h
 * @authors Manuel Gachs Ballegeer
 * 	    Gonzalo Moreno Soto
 */

#include<set>
#include<string>
#include"auxiliares.h"
#include"diccionario.h"
#include"ia.h"
#include"letra.h"

#ifndef _BUSCADOR_H
#define _BUSCADOR_H

/**
 * @brief Calcula las soluciones posibles de la IA
 * @param letras Conjunto de letras posibles
 * @param arbol Arbol de la ia
 * @return Un vector de soluciones
 */
std::vector<solucion> getSolucionesIA(const std::multiset<letra,classcomp_letra> &letras,arbol_ia &arbol);
/**
 * @brief Comprueba si una letra está en un conjunto de letras
 * @param _letra Caracter que se quiere comprobar
 * @param conjunto Conjunto de letras. ES MODIFICADO
 * @retval @retval true Si la letra está en el conjunto
 * 	   @retval false En caso contrario
 * @post Elimina la letra del conjunto en caso de que esté
 */
bool isLetra(const char &_letra,std::multiset<letra,classcomp_letra> &conjunto);
/**
 * @brief Elimina letras de una palabra dependiendo del nivel del árbol.
 * 	  Esas letras se añaden a un conjunto de letras
 * @param prenivel Nivel del nodo anterior. ES MODIFICADO
 * @param nuevonivel Nivel del nodo actual
 * @param palabra Palabra que se va a modificar. ES MODIFICADO
 * @param conjunto Conjunto de letras donde se añaden las letras eliminadas
 */
void arreglarString(size_t &prenivel,const size_t &nuevonivel,std::string &palabra,std::multiset<letra,classcomp_letra> &conjunto);
/**
 * @brief Devuelve la puntuación de la palabra a partir del nodo
 * @param nodo Nodo a partir del cual se calcula la puntuación
 * @return Puntuación de la palabra
 */
size_t puntosNodo(const nodo_ia* nodo);

#endif
