/**
 * @file auxiliares.h
 * @author Manuel Gachs Ballegeer
 *	   Gonzalo Moreno Soto
 */

#include<set>
#include<string>
#include<vector>
#include"bolsa_letras.h"
#include"diccionario.h"
#include"ia.h"
#include"letra.h"

#ifndef _AUXILIARES_H
#define _AUXILIARES_H

/**
 * @brief TDA Solución. 
 * 	  Representa una cadena de caracteres con la solución y su puntuación asociada
 */
struct solucion {
	std::string palabra; ///< Palabra de la solución
	int puntuacion; ///< Puntuación de la palabra
	/**
	 * @brief Sobrecarga del operador de asignación
	 * @param sol Solución a asignar
	 * @return La solución asignada
	 */
	solucion& operator=(const solucion &sol);
};
/**
 * @brief Genera un objeto solucion a partir de una cadena de caracteres
 * @param Cadena de caracteres
 * @param conjunto_letras Conjunto de letras de la que se obtiene la puntuación
 * @param dic Diccionario de las palabras posibles
 * @param longitud Si las palabras se puntúan por su longitud
 * @return El objeto solucion
 * @post En caso de ser una palabra imposible, la puntuación es -1
 */
solucion getSolucion(const std::string &_palabra,std::multiset<letra,classcomp_letra> &conjunto_letras,const diccionario &dic,bool longitud);
/**
 * @brief Muestra las soluciones encontradas
 * @param sols Conjunto de soluciones
 */
void printSoluciones(const std::vector<solucion> &sols);
/**
 * @brief Muestra un mensaje de ayuda en caso de error
 */
void help();
/**
 * @brief Genera un conjunto de letras para jugar
 * @param bl Bolsa de letras de las que se saca el conjunto
 * @param n Número de letras que componen el conjunto
 * @return Un conjunto de letras
 */
std::multiset<letra,classcomp_letra> getLetrasJuego(const bolsa_letras &bl,const size_t &n);
/**
 * @brief Imprime el conjunto de letras con el que se va a jugar
 * @param lj Conjunto de letras que se imprime
 */
void printLetrasJuego(const std::multiset<letra,classcomp_letra> &lj);
/**
 * @brief Comprueba si una palabra es posible
 * @param palabra La palabra que se quiere comprobar
 * @param dic Diccionario con el que comparar la palabra
 * @return @retval true Si la palabra está en el diccionario
 * 	   @retval false En caso contrario
 */
bool isPalabra(const std::string &palabra,const diccionario &dic);
/**
 * @brief Comprueba si una palabra se puede generar a partir de un conjunto de letras
 * @param palabra La palabra que se quiere comprobar
 * @param conjunto_letras Conjunto de letras que se pueden usar
 * @return @retval true Si se puede generar la palabra
 * 	   @retval false En caso contrario
 */
bool isPossible(const std::string &palabra,std::multiset<letra,classcomp_letra> &conjunto_letras);
/**
 * @brief Calcula la puntuación de una palabra
 * @param palabra Palabra de la que se quiere calcular la puntuación
 * @param equal Si todas las letras tienen la misma puntuación
 * @return La puntuación de la palabra
 */
size_t getPuntuacion(const std::string &palabra,bool equal);

#endif
