/**
 * @file bolsa_letras.h
 * @authors Manuel Gachs Ballegeer
 * 		    Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<set>
#include<vector>
#include"letra.h"
#include"lista_letras.h"

#ifndef _BOLSA_LETRAS_H
#define _BOLSA_LETRAS_H

/**
 * @brief Función para personalizar el orden de los multiset de letras
 */
struct classcomp_letra {
	bool operator()(const letra &lhs,const letra &rhs) const {
		return lhs.getChar()<rhs.getChar();
	}
};
/**
 * @brief TDA bolsa_letras
 */
class bolsa_letras {
private:
	std::vector<letra> bolsa; ///< Vector donde se almacenan las letras
	void shuffle(); ///< Función que mezcla las letras
public:
	/**
	 * @brief Constructor por defecto
	 */
	bolsa_letras() {};
	/**
	 * @brief Constructor por copia
	 * @param orig Bolsa de letras que se va a copiar
	 */
	bolsa_letras(const bolsa_letras &orig);
	/**
	 * @brief Constructor primitivo
	 * @param ll Lista de letras a partir de la que construir la bolsa
	 */
	bolsa_letras(const lista_letras &ll);
	/**
	 * @brief Saca un número de letras de la bolsa con reemplazamiento
	 * @param n Número de letras que se quieren sacar
	 * @return El conjunto de letras obtenidas
	 * @pre Es necesario generar una semilla la primera vez que se ejecute
	 */
	std::multiset<letra,classcomp_letra> sample(const size_t &n) const;
};

#endif
