/**
 * @file ia.h
 * @brief Implementación de la clase ia
 */

#include "letra.h"
#include "diccionario.h"
#include <vector>
#include <string>
#include <iostream>
#include <stack>
#include <algorithm>

#ifndef _IA_H
#define _IA_H

struct nodo_ia;
class arbol_ia;
class arbol_ia_iterator;

bool esNodoMayor(nodo_ia a, nodo_ia b);

/**
 * @brief TDA nodo_ia. Representa un nodo de un arbol n-ario
 */
struct nodo_ia {
	letra id; 		///< Objeto letra del nodo
	int mejor;		///< Mejor puntuacion alcanzable desde ese nodo
	unsigned int nivel;	///< Nivel del profundidad dentro del árbol
	nodo_ia* padre;		///< Puntero al nodo padre
	std::vector<nodo_ia> hijos;	///< Set de nodos hijos

	/**
	 * @brief Constructor por defecto de nodo_ia
	 */
	nodo_ia();

	/**
	 * @brief Constructor por parámetros de nodo_ia
	 */
	nodo_ia(const letra _id, nodo_ia* _padre);

	/**
	 * @brief Añade un nodo hijo
	 */
	void addNodo(letra _letra);
};

/**
 * @brief TDA arbol_ia, representa un arbol n-ario compuesto por nodos nodo_ia
 */
class arbol_ia {
private:
	nodo_ia raiz;		///< Nodo raíz del árbol

public:
	/**
	 * @brief Constructor por parámetros de arbol_ia
	 * @param dic Diccionario a partir del que creamos el árbol
	 */
	arbol_ia(diccionario dic);

	/**
	 * @brief Devuelve el nodo raíz
	 * @return Nodo raíz del arbol
	 */
	nodo_ia* getRaiz();

	/**
	 * @brief Método para añadir palabras al árbol
	 * @param palabra Palabra que añadiremos
	 */
	void addPalabra(std::string palabra);

	/**
	 * @brief Ordena el árbol
	 * El arbol debe de estar ordenado dejando los nodos
	 * con mayor valor de 'mejor' a la izquierda
	 */
	void ordenar();

	/**
	 * @brief Permite puntuar las palabras por longitud y no por puntuacion de letra
	 */
	void puntuarLongitud();

	/**
	 * @brief Muestra el árbol en pantalla
	 */
	void mostrar();
};

/**
 * @brief TDA arbol_ia_iterator, representa un iterador de arbol_ia
 *
 * El iterador tiene sobrecargados los operadores sufijos ++ y --, el funcionamiento
 * es el siguiente:
 * 	Usar el operador ++ prosigue por el camino que estamos recorriendo,
 * 	descendiendo un nivel hacia abajo en el árbol, por el contrario el 
 * 	operador -- pasa al siguiente nodo hermano, si nos encontramos en el
 * 	último nodo hermano, pasa al siguiente hermano del nodo padre.
 */
class arbol_ia_iterator {
private:
	nodo_ia* nodo;			///< Nodo del árbol en el que se encuentra el iterador
	std::stack<unsigned int> posicionVertical;
public:
	/**
	 * @brief Constructor por parámetros de la clase arbol_ia_iterator
	 */
	arbol_ia_iterator(arbol_ia arbol);

	/**
	 * @brief Sobrecarga del operador ++
	 */
	arbol_ia_iterator& operator++(int x);

	/**
	 * @brief Sobrecarga del operador --
	 */
	arbol_ia_iterator& operator--(int x);

	/**
	 * @brief Metodo para que el iterador apunte al padre del nodo
	 */
	arbol_ia_iterator& apuntaPadre();

	/**
	 * @brief Sobrecarga del operador *
	 */
	nodo_ia* operator*();

	friend arbol_ia;
};

#endif
