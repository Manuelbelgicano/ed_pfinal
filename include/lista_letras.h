/**
 * @file lista_letras.h
 * @authors Manuel Gachs Ballegeer
 *			Gonzalo Moreno Soto
 * @license GPLv3
 */

#include<string>
#include<set>
#include"letra.h"
#include"diccionario.h"

#ifndef _LISTA_LETRAS_H
#define _LISTA_LETRAS_H

/**
 * @brief TDA datos_letra. Representa a una letra con una cantidad de apariciones asociada
 */
struct datos_letra {
	letra l;		///< Letra
	unsigned int c;		///< Cantidad de veces que aparece la letra
	/**
	 * @brief Sobrecarga del operador de asignación de datos_letra
	 * @param a Objeto que se asigna
	 * @return El objeto asignado
	 */
	datos_letra operator=(const datos_letra &a);
};
/**
 * @brief Suma dos objetos del tipo datos_letra
 * @param a Primer objeto. ES MODIFICADO
 * @param b Segundo objeto
 * @return La suma de los dos objetos
 * @pre Las letras deben de ser iguales. En caso contrario no se hace nada
 */
datos_letra& operator+(datos_letra &a,const datos_letra &b);
/**
 * @brief Función para personalizar el orden de lista_letras
 */
struct classcomp_datosletra {
	bool operator()(const datos_letra &lhs,const datos_letra &rhs) const {
		return lhs.l.getChar()<rhs.l.getChar();
	}
};
/**
 * @brief TDA lista_letras. Representa un conjunto de datos_letra
 */
class lista_letras {
private:
	std::set<datos_letra,classcomp_datosletra> datos; ///< Lista de datos de letras
public:
	/**
	 * @brief Constructor por defecto
	 */
	lista_letras() {}
	/**
	 * @brief Constructor por copia
	 * @param orig Lista de letras que se copia
	 */
	lista_letras(const lista_letras &orig);
	/**
	 * @brief Consulta el tamaño de la lista
	 * @return El tamaño de la lista
	 */
	size_t size() const {return datos.size();}
	/**
	 * @brief Asigna una lista de letras a otra
	 * @param ll Lista de letras que se quiere asignar
	 * @return La lista de letras asignada
	 */
	lista_letras& operator=(const lista_letras &ll);
	/**
	 * @brief Iteradores de la clase
	 */
	typedef std::set<datos_letra,classcomp_datosletra>::iterator iterator;
	/**
	 * @brief Devuelve un iterador al comienzo de la lista
	 * @return Un iterador al inicio de la lista
	 */
	iterator begin() {return datos.begin();}
	/**
	 * @brief Devuelve un iterador al final de la lista
	 * @return Un iterador al final de la lista
	 */
	iterator end() {return datos.end();}
	/**
	 * @brief Iteradores constantes de la clase
	 */
	typedef std::set<datos_letra,classcomp_datosletra>::const_iterator const_iterator;
	/**
	 * @brief Devuelve un iterador al comienzo de la lista
	 * @return Un iterador al inicio de la lista
	 */
	const_iterator cbegin() const {return datos.cbegin();}
	/**
	 * @brief Devuelve un iterador al final de la lista
	 * @return Un iterador al final de la lista
	 */
	const_iterator cend() const {return datos.cend();}
	/**
	 * @brief Comprueba si una letra está en la lista
	 * @param l Letra que se quiere buscar
	 * @param it Iterador a la posición de la letra
	 * @return @retval true Si esta la letra
	 * 	   @retval false En caso contrario
	 */
	bool isLetter(const char _l,lista_letras::iterator &it);
	/**
	 * @brief Añade una letra a la lista
	 * @param l Letra que se añade
	 * @param c Cantidad de veces que aparece la letra
	 * @post Si la letra ya existia, se suman sus cantidades
	 */
	void insertLetter(const letra &_l,unsigned int _c);
	/**
	 * @brief Suma dos listas de letras
	 * @param ll Lista de letras que se quiere sumar
	 * @return La suma de las dos listas
	 */
	lista_letras& operator+(const lista_letras &ll);
	/**
	 * @brief Sobrecarga del operador de salida
	 */
	friend std::ostream& operator<<(std::ostream &os,const lista_letras &ll);
	/**
	 * @brief Guarda una lista de letras en un archivo
	 * @param file Fichero donde se quiere guardar la lista
	 * @return @retval true Si ha realizado la operación con éxito
	 * 	   @retval false En caso contrario
	 */
	bool toFile(const char file[] = "salida.txt");
};

std::ostream& operator<<(std::ostream &os,const lista_letras &ll);
/**
 * @brief Crea una lista a partir de una cadena de caracteres
 * @param s Cadena de caracteres de la que se quiere obtener la lista
 * @return La lista de letras
 */
lista_letras fromString(const std::string &s);
/**
 * @brief Crea una lista a partir de un diccionario
 * @param dic Diccionario del que se quiere obtener la lista
 * @return La lista de letras
 */
lista_letras fromDic(const diccionario &dic);
/**
 * @brief Crea una lista de letras a partir de un archivo
 * @param file Archivo del que se quiere obtener la lista
 * @return Una lista de letras
 */
lista_letras fromDicFile(const char file[]);

#endif
